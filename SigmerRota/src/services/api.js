import axios from 'axios';

const api = axios.create({
    baseURL: 'https://sigmer.herokuapp.com/'
});

export default api;