import { createStackNavigator } from 'react-navigation';

import Login from '../pages/login/login';
import Principal from '../pages/telaPrincipal/telaPrincipal';
import SituacaoMonitorado from '../pages/situacaoMonitorado/situacaoMonitorado';
import GerarOcorrencia from '../pages/ocorrenciaTornozeleira/ocorrenciaTornozeleira';
import Camera from '../services/camera';

export default createStackNavigator({
    Login,
    Principal,
    SituacaoMonitorado,
    GerarOcorrencia,
    Camera
}
);