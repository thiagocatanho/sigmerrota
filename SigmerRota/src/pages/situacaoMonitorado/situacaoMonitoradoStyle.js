import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F7F7F7",
        padding: 10,
        width: '100%',
        height: '100%',
    },
    logoSeres: {
        width: '100%',
        height: '30%',
        resizeMode: 'contain',
        justifyContent: 'center',
    },
    botaoPesquisar: {
        height: 42,
        width: '25%',
        flex: 0.3,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#6E88B5',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    botaoGerarOcorrencia: {
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    inputPesquisa: {
        flex: 0.7,
        height: 42,
        width: '70%',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: "#FFF"
    },
    textoBotao: {
        fontSize: 16,
        color: '#FFF',
        fontWeight: 'bold'
    },
    containerPesquisa: {
        flexDirection: 'row',
        padding: 10
    }, 
    containerDadosPesquisa: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        width: '90%',
        marginLeft: 10,
        marginBottom: 10,
        marginTop: 20
    },
    textoDados: {
        marginBottom: 5,
        fontSize: 16
    }
});

export default styles;