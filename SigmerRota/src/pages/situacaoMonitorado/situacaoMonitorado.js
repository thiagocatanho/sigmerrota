import React, { Component } from 'react';
import api from '../../services/api';
import Style from './situacaoMonitoradoStyle';

import { View, Text, TextInput, TouchableOpacity, Image, BackHandler, Alert } from 'react-native';

export default class SituacaoMonitorado extends Component {

    constructor(props){
        super(props);
        this.state = {
            prontuarioPesquisa: '',
            prontuario: '',
            nome: '',
            unidadePrisional: '',
            situacao: ''
        }
    } 

    consultarMonitorado = async () => {
        const { prontuarioPesquisa } = this.state;

        try{
            const result = await api.get(`/monitorado/consultarMonitorado/${prontuarioPesquisa}`); 
            
            if(result.data.erro){

                this.setState({
                    nome: '', 
                    prontuario: '',
                    situacao: '',
                    unidadePrisional: ''  
                });

                Alert.alert(
                    'Erro',
                    result.data.erro
                );
            } else {
                this.setState({
                    nome: result.data.nome, 
                    prontuario: result.data.prontuario,
                    situacao: result.data.situacao,
                    unidadePrisional: result.data.unidadePrisional  
                });
            }
        } catch (error) {
            console.log(error);
        }
    }

    gerarOcorrencia = async () => {
        const { prontuario } = this.state;
        this.props.navigation.navigate('GerarOcorrencia', {prontuario: prontuario});
    }
    
    render() {
        return (
            <View style={ Style.container }>
                
                <Image 
                    style={ Style.logoSigmer } 
                    source={{ uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/ae186ab45d5eb1692c3edc48cd35ffd5/272481f6-792b-466c-8d40-4e7bf6669862.jpg'}}
                />

                <View style = { Style.containerPesquisa }>
                    <TextInput 
                        style = { Style.inputPesquisa }
                        placeholder = "Prontuario"
                        value = {this.state.prontuarioPesquisa}
                        onChangeText = { prontuarioPesquisa => this.setState({ prontuarioPesquisa })}
                    />
                    <TouchableOpacity 
                        style = { Style.botaoPesquisar }
                        onPress = { this.consultarMonitorado } 
                    >
                        <Text style = { Style.textoBotao } >Pesquisar</Text>
                    </TouchableOpacity>
                </View>

                <View style = { Style.containerDadosPesquisa }>
                    <Text style = { Style.textoDados }> Nome: { this.state.nome } </Text>
                    <Text style = { Style.textoDados }> prontuario: { this.state.prontuario } </Text>
                    <Text style = { Style.textoDados }> Unidade Prisional: { this.state.unidadePrisional} </Text>
                    <Text style = { Style.textoDados }> Situação: { this.state.situacao } </Text>
                </View>

                <TouchableOpacity 
                    style = { Style.botaoGerarOcorrencia }
                    onPress = { this.gerarOcorrencia.bind() }
                >
                    <Text style = { Style.textoBotao } >Gerar Ocorrencia</Text>
                </TouchableOpacity>

                <Image 
                    style = { Style.logoSeres }
                    resizeMode="contain"
                    source={{
                    uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/82f69706637d9428a063fa686454fb6c/c94a56de-f509-4892-9f61-fe924e8cb126.jpg'}}
                />

            </View>
        );
    }
}