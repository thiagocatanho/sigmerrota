import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F7F7F7",
        padding: 5,
        width: '100%',
        height: '100%',
    },
    logoSeres: {
        width: '100%',
        height: '30%',
        resizeMode: 'contain',
        justifyContent: 'center',
        paddingHorizontal: 20,
        alignSelf: 'center',
    },
    inputTextos: {
        height: 42,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: "#FFF",
        marginTop: 10
    },
    containerBtn: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnOk: {
        height: 42,
        width: '25%',
        flex: 0.3,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#6E88B5',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    btnCancelar: {
        height: 42,
        width: '25%',
        flex: 0.3,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    textBtn: {
        fontSize: 16,
        color: '#FFF',
        fontWeight: 'bold'
    },
    btnTirarFoto: {
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    imgTornozeleira: {
        width: '100%',
        height: '30%',
        borderRadius: 5,
        marginTop: 10
    }


})

export default styles;