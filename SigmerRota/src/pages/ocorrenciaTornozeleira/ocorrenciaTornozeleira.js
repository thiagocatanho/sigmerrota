import React, { Component } from 'react';
import Style from './ocorrenciaTornozeleiraStyle';
import { View, Text, TextInput, TouchableOpacity, Image, Alert } from 'react-native';

export default class OcorrenciaTornozeleira extends Component {

    static navigationOptions = { };

    constructor(props) {
        super(props);
        this.state = {
            imagemTornozeleira: '',
            observacao: '',
            numeroTornozeleira: '',
        }
        this.abrirCamera
    }

    abrirCamera = () => { 
        this.props.navigation.navigate('Camera');
    }

    gerarOcorrenciaAction = () => {
        if (this.state.imagemTornozeleira === ''){
            return Alert.alert('Erro','Necessario anexar uma imagem a ocorrencia !!!');
        }

        if (this.state.numeroTornozeleira === ''){
            return Alert.alert('Erro', 'Nessario informar o numero da tornozeleira !!!');
        }

        if (this.state.observacao === ''){
            return Alert.alert('Erro', 'Favor informar alguma observação a Ocorrencia !!!');
        }

        Alert.alert('Sucesso', 'Ocorrencia gerada com Sucesso !!!');

        this.props.navigation.navigate('Principal');

    }

    componentDidUpdate(){
        if (this.props.navigation.state.params) {
            if(this.props.navigation.state.params.imagem !== this.state.imagemTornozeleira){
                this.setState({ imagemTornozeleira: this.props.navigation.state.params.imagem })
            }
        }
    }

    renderImagem = () => {
        if (this.state.imagemTornozeleira === ''){
            return (
                <Image
                    style = { Style.imgTornozeleira }
                    resizeMode="contain"
                    source={{
                        uri: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/gualambo/sem-imagem-1.jpg/@@images/c4f9f09c-1e96-42d6-bd83-f5c47ee1a638.jpeg' }}
                />
            )
        } else {
            return (
                <Image
                    style = { Style.imgTornozeleira }
                    resizeMode="contain"
                    source={{
                        uri: this.state.imagemTornozeleira }}
                />
            )
        }
    }
    
    render() {
        return (
            <View style = { Style.container} >
                <TextInput 
                    style = { Style.inputTextos }
                    placeholder = 'Nº da Tornozeleira'
                    onChangeText = { numeroTornozeleira => this.setState({ numeroTornozeleira })}
                />
                <TextInput 
                    style = { Style.inputTextos }
                    placeholder = 'Observações'
                    onChangeText = { observacao => this.setState({ observacao })}
                />

                { this.renderImagem() }

                <View>
                    <TouchableOpacity style = { Style.btnTirarFoto } onPress = { this.abrirCamera }> 
                        <Text style = { Style.textBtn }> Tirar Foto Tornozeleira </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style = { Style.btnTirarFoto }
                        onPress = { this.gerarOcorrenciaAction }
                    >
                        <Text style = { Style.textBtn }> Gerar Ocorrencia </Text>
                    </TouchableOpacity>

                </View>

                <Image
                    style = { Style.logoSeres }
                    resizeMode="contain"
                    source={{
                    uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/82f69706637d9428a063fa686454fb6c/c94a56de-f509-4892-9f61-fe924e8cb126.jpg'}}
                />
            </View>
        );
    }
    
}