import { StyleSheet } from 'react-native';

const Style = StyleSheet.create({
    logoSigmer: {
        width: '100%',
        height: '30%',
        resizeMode: 'contain',
        marginBottom: 30,
        justifyContent: 'center',
        borderWidth: 1
    },
    container: {
        flex: 1,
        backgroundColor: "#F7F7F7",
        padding: 10
    },
    botaoPrincipal: {
        height: 42,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    textoBotaoPrincipal: {
        fontSize: 14,
        color: '#FFF',
        fontWeight: 'bold'
    },
    dadosUsuario: {
        // flex: 1,
        flexDirection: 'row',
        height: 50,
        padding: 2
    },
    textoUsuario: {
        width:'80%',
        fontSize: 14,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textoUsuarioDados: {
        fontStyle: 'italic',   
        fontWeight: 'bold'
    },
    botaoDeslogar: {
        width: '20%',
        marginLeft: 5
    },
    botaoDeslogarImagem: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    }

});

export default Style;