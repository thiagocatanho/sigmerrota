import React, { Component } from 'react';
import Style from './telaPrincipalStyle';

import { TouchableOpacity, View, Text, Image, BackHandler, Alert } from 'react-native';


export default class TelaPrincipal extends Component {

    static navigationOptions = { 
        title: 'Tela Principal',
        headerLeft: null
    };
    
    consultarSituacaoMonitorado = () => {
        this.props.navigation.navigate('SituacaoMonitorado');
    }

    gerarOcorrenciaTornozeleira = () => {
        this.props.navigation.navigate('GerarOcorrencia');
    }

    logout = () => {
        Alert.alert(
            'Logout',
            'Deseja Realmente Sair?',
            [
                { text: 'Não'}, 
                { text: 'Sim', onPress: () => this.props.navigation.navigate('Login')}
            ],
            { cancelable: false}
        );
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', function () {
          return true
        })
      } 

    render(){
        return (
            <View style={ Style.container }>

                <View style={ Style.dadosUsuario }>
                    <View style={ Style.textoUsuario }>
                        <Text>
                            Usuario: <Text style={ Style.textoUsuarioDados }>{this.props.navigation.state.params.usuario.usuario_nome}</Text>
                        </Text> 

                        <Text>
                            Matricula: <Text style={ Style.textoUsuarioDados }>{this.props.navigation.state.params.usuario.usuario_matricula}</Text>
                        </Text>
                    </View> 

                    <TouchableOpacity style={ Style.botaoDeslogar } onPress={ this.logout }>
                        <Image 
                            style={ Style.botaoDeslogarImagem }
                            source={{ uri: 'https://www.shareicon.net/download/2016/12/12/862833_close_512x512.png'}}
                        />
                    </TouchableOpacity>

                </View> 

                <Image style={ Style.logoSigmer } source={{ uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/ae186ab45d5eb1692c3edc48cd35ffd5/272481f6-792b-466c-8d40-4e7bf6669862.jpg'}} />

                <TouchableOpacity
                    style={ Style.botaoPrincipal }
                    onPress={ this.consultarSituacaoMonitorado }
                >
                    <Text style={ Style.textoBotaoPrincipal }>Consultar Situação do Monitorado</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={ Style.botaoPrincipal }
                    onPress={ this.gerarOcorrenciaTornozeleira}
                >
                    <Text style={ Style.textoBotaoPrincipal }>Gerar Ocorrência de Tornozeleira</Text>
                </TouchableOpacity>

            </View>
        );
    }

}