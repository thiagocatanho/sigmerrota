import React, { Component } from 'react';
import api from '../../services/api';
import styles from './loginStyle';
import NetInfo from '@react-native-community/netinfo';

import { View, TextInput, Text, Image, TouchableOpacity, Alert } from 'react-native';

export default class login extends Component {

    static navigationOptions = { };

    state = {
        matricula: '',
        senha: ''
    };

    realizarLogin = async () => {
        
        const { matricula, senha } = this.state;

        try {
            const result = await api.post(`/usuario/login`, {
                matricula: matricula,
                senha: senha
            });
            this.props.navigation.navigate('Principal', { usuario: result.data});

        } catch (error) {

            NetInfo.fetch().then(state => {
                if (!state.isConnected || state.type == 'none'){
                    Alert.alert(
                        'Erro',
                        'Sem Acesso a Internet'
                    );
                } else {
                    Alert.alert(
                        'Erro',
                        'Login e/ou Senha Invalidos'
                    );
                }    
            });
            
            
        }
        
    }

    render() {
        return(
            <View style={styles.container}>

                <Image style={styles.logoSigmer} source={{
                    uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/ae186ab45d5eb1692c3edc48cd35ffd5/272481f6-792b-466c-8d40-4e7bf6669862.jpg'}} /> 
                <TextInput 
                    style = {styles.formulario}
                    placeholder = "Matricula"
                    value = {this.state.matricula}
                    onChangeText = { matricula => this.setState({ matricula })}
                /> 

                <TextInput 
                    style = {styles.formulario}
                    placeholder = "Senha"
                    secureTextEntry = {true}
                    value = { this.state.senha}
                    onChangeText = { senha => this.setState({ senha })}
                />

                <TouchableOpacity
                    style = {styles.botaoLogar}
                    onPress = { this.realizarLogin }
                >
                    <Text style={styles.botaoLoginText}>Login</Text>
                </TouchableOpacity>


                <Image 
                    style={styles.logoSeres} 
                    resizeMode="contain"
                    source={{
                    uri: 'https://trello-attachments.s3.amazonaws.com/5cb336aed95fe029d8a9a451/5cb3393e84caab5082766f08/82f69706637d9428a063fa686454fb6c/c94a56de-f509-4892-9f61-fe924e8cb126.jpg'}}
                    /> 

            </View>
        )
    }
}