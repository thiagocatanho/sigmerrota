import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F7F7F7",
        padding: 10
    },
    formulario: {
         height: 40,
         borderWidth: 1,
         borderRadius: 5,
         marginBottom: 10,
         backgroundColor: "#FFF"
    },
    logoSigmer: {
        width: '100%',
        height: '30%',
        resizeMode: 'contain',
        marginBottom: 20,
        justifyContent: 'center'
    },
    logoSeres: {
        width: '100%',
        height: '30%',
        resizeMode: 'contain',
        justifyContent: 'center',
    },
    botaoLogar: {
        height: 42,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: '#6E88B5',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    botaoLoginText: {
        fontSize: 20,
        color: '#FFF',
        fontWeight: 'bold'
    }
});

export default Styles;